package controle;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import modelo.Medico;
import modelo.MedicoDAO;

@Controller
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private MedicoDAO medicoDAO;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String listar(Model model) {
		List<Medico> medicos = medicoDAO.findAll();
		model.addAttribute("lista_medico", medicos);
		return "medicos/lista";
	}

	@RequestMapping(value = "/alterar", method = RequestMethod.GET)
	public String alterar(@RequestParam Integer codigo, Model model) {
		Medico medico = medicoDAO.getOne(codigo);
		model.addAttribute("medico", medico);
		return "medicos/form";
	}

	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo(Medico medico, Model model) {
		model.addAttribute("medico", medico);
		return "medicos/form";
	}

	@RequestMapping(value = "/novo1", method = RequestMethod.GET)
	public ModelAndView novo1(Medico medico) {
		ModelAndView modv = new ModelAndView("medicos/form");
		modv.addObject("medico", medico);
		return modv;
	}

	@RequestMapping(value = "/gravarMedico", method = RequestMethod.POST)
	public String gravar(Medico medico, Model model) {
		medicoDAO.saveAndFlush(medico);
		return this.listar(model);
	}

	@RequestMapping(value = "/gravarMedico1", method = RequestMethod.POST)
	public String gravar1(@Valid Medico medico, BindingResult result) {
		// verifica se existem erros na valida��o
		if (result.hasErrors()) {
			return "medicos/form";
		} else {
			medicoDAO.saveAndFlush(medico);
			return "redirect:/medicos/";
		}
	}

	@RequestMapping(value = "/gravarMedico2", method = RequestMethod.POST)
	public String gravar2(@RequestParam Integer codigo, @RequestParam String nome, @RequestParam String crm) {

		Medico medico = new Medico();
		medico.setNome(nome);
		medico.setCodigo(codigo);
		medico.setCrm(crm);

		medicoDAO.saveAndFlush(medico);
		return "redirect:/medicos/";
	}

	@RequestMapping(value = "/gravarMedico3", method = RequestMethod.POST)
	public String gravar3(@RequestParam Map<String, String> params) {

		Medico medico = new Medico();
		medico.setNome(params.get("nome"));
		medico.setCodigo(Integer.parseInt(params.get("codigo")));
		medico.setCrm(params.get("crm"));

		medicoDAO.saveAndFlush(medico);
		return "redirect:/medicos/";
	}

	public MedicoDAO getMedicoDAO() {
		return medicoDAO;
	}

	public void setMedicoDAO(MedicoDAO medicoDAO) {
		this.medicoDAO = medicoDAO;
	}

}

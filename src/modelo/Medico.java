package modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="tab_medicos")
public class Medico {

	@NotNull(message = "O C�DIGO n�o pode ser vazio.")
	@DecimalMin(value = "1", message = "O C�DIGO deve ser um n�mero inteiro v�lido.")
	@Id
	private Integer codigo;

	@NotEmpty(message = "O NOME n�o pode ser vazio.")
	@Size(min = 5, message = "O nome n�o pode ter menos que 5 caracteres!")
	private String nome;

	@NotEmpty(message = "O CRM n�o pode ser vazio.")
	@Size(min = 5, message = "O CRM n�o pode ter menos que 5 caracteres!")
	@DecimalMin(value = "1", message = "O CRM deve ser um n�mero inteiro v�lido.")
	private String crm;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

}

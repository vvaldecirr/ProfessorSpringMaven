package modelo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicoDAO extends JpaRepository<Medico, Integer> {
	
	public List<Medico> findByNome(String nome);
	
	public List<Medico> findByCrm(String crm); 
	
	
}
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css" media="screen">
	 @import url("<c:url value="/resources/css/main.css"/>");
</style>

<!-- link rel="stylesheet" href="resources/css/main.css" -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Medicina e Saúde</title>
</head>
<body>

	<%@ include file="/WEB-INF/views/head.jsp"%>

	<c:url var="url" value="/medicos/gravarMedico" />
	<form:form method="post" action="${url}" modelAttribute="medico">
		<p>
			<form:errors path="nome" style="color: red" /><br />
			Nome do médico:
			<form:input type="text" path="nome" size="80" />
		</p>
		
		<p>
			<form:errors path="crm" style="color: red" /><br />
		   CRM do médico:
		   <form:input type="text" path="crm" size="80" />
		</p>
		
		<p>
			<form:errors path="codigo" style="color: red" /><br />
		   Código:
		   <form:input type="text" path="codigo" size="80" />
		</p>
		
		<p> <input type="submit" value="gravar"> </p>



	</form:form>



</body>
</html>